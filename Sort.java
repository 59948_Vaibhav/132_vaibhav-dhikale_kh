import java.util.*;
class Sort{
	
	static void Insertionsort(int a1[])
	{
		int n=a1.length;
		for(int i=1;i<n;i++)
		{   
			int k=a1[i];
			int j=i-1;
			
			while(j>=0 && a1[j]>k)
			{
				a1[j+1]=a1[j];
				j=j-1;
				Display(a1);
				System.out.println("");
			}
			a1[j+1]=k;
		}
		
	}
	
	static void Display(int a1[])
	{
		int n=a1.length;
		for(int i=0;i<n;i++)
		{
			System.out.print(a1[i]+" ");
		}
	}

	
	public static void main(String args[])
	{
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int []a1=new int[n];
		for(int i=0;i<n;i++)
		{
			a1[i]=sc.nextInt();
		}
		Insertionsort(a1);
		Display(a1);
		
	
	}
		
}